Vue.component('weather-search', {
    props: ['initialterm'],
    data: function() {
        return {
            term: this.initialterm,
            resultData: []
        }
    },
    methods: {
        doSearch: function() {
            axios.get(APP_URL + "/weather.php", {
                responseType: 'json',
                params: {
                    command: 'search',
                    keyword: this.term
                }
            })
            .then((response)  =>  {
                this.resultData = response.data;                            
            })
        }
    },
    created: function() {
        this.doSearch();
    },
    template: "<div>\
        <input type='text' class='w-80 p-1' v-model='term' placeholder='enter a search term...'/>\
        <button class='btn-primary' v-on:click='doSearch'>Go</button>\
        <div v-if='resultData.length === 0'>No results were found. Try changing the keyword!</div>\
        <div><weather v-for='data in resultData' :key='data.woeid' :locationid='data.woeid' summary></weather></div>\
    </div>"
})

Vue.component('weather-details', {
    props: {
        d: Object,
        locationid: String
    },
    computed: {
        title: function() {
            if (this.d.summary === true) {
                return this.d.title;
            } else {
                // get name of day                
                let date = new Date( this.d.applicable_date );                
                let dayName = date.toLocaleDateString('en-US', { weekday: 'long' });;
                
                return dayName + ' ' + this.d.applicable_date;
            }
        },
        imageUrl: function() {
            return 'https://www.metaweather.com/static/img/weather/' + this.d.weather_state_abbr + '.svg'
        },
        detailUrl: function() {
            return APP_URL + '/weather/' + this.locationid
        }        
    },
    template: "<div><h3>{{ title }}</h3>\
    <div class='row'>\
        <div class='col-sm-3'><img :src='imageUrl'/></div>\
        <div class='col-sm-5'>\
            <div v-if='d.summary === true'>Now: {{ d.the_temp.toFixed(2) }}° C</div>\
            <div v-else></div>\
            <div>Low: {{ d.min_temp.toFixed(2) }}° C</div>\
            <div>High: {{ d.max_temp.toFixed(2) }}° C</div>\
            <div v-if='d.summary === true'><a :href='detailUrl'>Details</a></div>\
        </div>\
    </div>\
    </div>"
})

Vue.component('weather', {    
    props: {
        summary: Boolean,
        locationid: String
    },
    data: function() {
        return {                        
            consolidated_data: {},
            locationName: ''
        }
    },
    computed: {        
        detailUrl: function() {
            return APP_URL + '/weather/' + this.locationid
        }        
    },
    created: function() {
        axios.get(APP_URL + "/weather.php", {
            responseType: 'json',
            params: {
                command: 'location',
                woeid: this.locationid
            }
        })
        .then((response)  =>  {                                            
            if (this.summary === true) {                
                this.consolidated_data = [response.data.consolidated_weather[0]];
                this.consolidated_data[0].title = response.data.title;
                this.consolidated_data[0].summary = true;
            } else {                
                this.consolidated_data = response.data.consolidated_weather;
                this.locationName = response.data.title;
            }
            
        }, (error)  =>  {
          alert('err:' + response);
        })     
    },
    template: "<div class='card p-3 m-1'>\
        <h1 v-if='summary != true'>{{ locationName }}</h1>\
        <weather-details\
            v-for='data in consolidated_data'\
            :key='data.woeid'\
            :d='data'\
            :locationid='locationid'\
    ></weather-details>\
    </div>" 
})

var vm = new Vue({
  el: '#app'
})

