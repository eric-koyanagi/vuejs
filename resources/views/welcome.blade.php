@extends('layouts/app')

@section('content')
    <div class="container col-sm-4 centered p-3">
        <div><strong>Search: </strong></div>
        
        <input type="text" id="search-term" name="term" placeholder="Enter a search term..."/>            
        <button id="search-sub" onClick="doSearch()" class="btn-primary">Go</button>
        
    </div>
    <div class="container col-sm-4 centered">
        <weather locationId="2344116" summary></weather>
        <weather locationId="638242" summary></weather>
        <weather locationId="44418" summary></weather>
        <weather locationId="565346" summary></weather>
        <weather locationId="560743" summary></weather>
        <weather locationId="9807" summary></weather>                 
    </div>
    <script>
        function doSearch() {
            window.location = APP_URL + "/search/" + encodeURI($("#search-term").val());
        }
        
    </script>
@endsection
