@extends('layouts/app')

@section('content')
    <div class="container col-sm-4 centered">
        <weather-search initialterm="{{$term}}"></weather-search>        
    </div>
@endsection