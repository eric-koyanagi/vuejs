@extends('layouts/app')

@section('content')
    <div class="container col-sm-4 centered">
        <weather locationId="{{ $woeid }}"></weather>
    </div>
@endsection